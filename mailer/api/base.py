# -*- coding: utf-8 -*-
"""
    mailer.api.base
    ~~~~~~~~~~~~~~~

    Base classes to be used across the mailer.api

    :copyright: © 2018 by Ilham Imaduddin.
"""

from abc import ABC, abstractmethod
from typing import Any, Dict

from jsonschema import validate, ValidationError

from mailer.api.exception import RequestError


class RequestData(ABC):
    """Abstract class to contain HTTP request data.

    All RequestData implementation should define the `schema` property and
    populate the instance data by calling the `load` method. That way all
    request data are validated according to the defined schema.

    Example:
    ```python
    class EmailPOSTData(RequestData):
        schema = {
            "type": "object",
            "properties": {"email": {"type": "string"}},
            "required": ["email"],
        }

        def __init__(self, data: Dict[str, str]) -> None:
            self.email = None

            self.load(data, request_name="email")
    ```

    """

    @property
    @abstractmethod
    def schema(self):
        """JSON schema of the received request data.

        Override this property by following the convention on
        http://json-schema.org
        """
        pass

    def load(self, data: Dict[str, Any], request_name: str = "request") -> None:
        """Load and validation dictionary-like data to the instance data.

        Arguments:
            data {Dict[str, any]} -- Dictionary containing the request data

        Keyword Arguments:
            request_name {str} -- Name of the request (default: {"request"})

        Raises:
            RequestError -- The request data is invalid.
        """

        try:
            validate(data, self.schema)
            self.__dict__ = data  # pylint: disable=attribute-defined-outside-init
        except ValidationError as err:
            raise RequestError(400, f"{request_name}/validation-error", err.message)
