# -*- coding: utf-8 -*-
"""
    tests.api.test_base
    ~~~~~~~~~~~~~~~~~~~

    Test cases for RequestData.

    :copyright: © 2018 by Ilham Imaduddin.
"""

from typing import Dict

import pytest

from mailer.api.base import RequestData
from mailer.api.exception import RequestError


class EmailPOSTData(RequestData):
    """An example of RequestData implementation."""

    schema = {
        "type": "object",
        "properties": {"email": {"type": "string"}},
        "required": ["email"],
    }

    def __init__(self, data: Dict[str, str]) -> None:
        self.email = None

        self.load(data, request_name="email")


def test_valid_data():
    """EmailPOSTData should contains the same data with it's raw data."""
    raw_data = {"email": "some@email.com"}
    data = EmailPOSTData(raw_data)

    assert data.email == raw_data["email"]


def test_invalid_data():
    """Invalid data should raise RequestError."""
    raw_data = {"name": "some name"}

    with pytest.raises(RequestError) as excinfo:
        EmailPOSTData(raw_data)

    assert excinfo.value.code == 400
    assert excinfo.value.status == "email/validation-error"
    assert "required" in excinfo.value.message


def test_invalid_data_type():
    """Invalid data type should raise RequestError."""
    raw_data = {"email": 5}

    with pytest.raises(RequestError) as excinfo:
        EmailPOSTData(raw_data)

    assert excinfo.value.code == 400
    assert excinfo.value.status == "email/validation-error"
    assert "string" in excinfo.value.message
