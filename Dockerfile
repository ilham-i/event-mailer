# There are two app in this image, the API and the task worker
#
# Run the API in development:
#   docker run --rm -d -p 5000:5000 event-mailer flask run
#
# Run the Task workers:
#   docker run --rm -d event-mailer celery -A mailer.tasks worker -l info

FROM python:alpine

LABEL Name=event-mailer Version=0.0.1

WORKDIR /app
EXPOSE 5000

RUN apk add --no-cache postgresql-dev gcc python3-dev musl-dev

COPY . /app

RUN python3 -m pip install pipenv && pipenv install --ignore-pipfile

ENV FLASK_APP=mailer/api/app.py
ENV FLASK_RUN_HOST=0.0.0.0
ENV FLASK_RUN_PORT=5000

ENTRYPOINT ["pipenv", "run"]
