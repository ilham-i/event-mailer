# -*- coding: utf-8 -*-
"""
    mailer.api.exception
    ~~~~~~~~~~~~~~~~~~~~

    Define exceptions that could be occured in the app.

    :copyright: © 2018 by Ilham Imaduddin.
"""

from flask import Response, jsonify


class RequestError(Exception):  # pylint: disable=missing-docstring
    def __init__(self, code: int, status: str, message: str) -> None:
        """RequestError constructor.

        Arguments:
            code {int} -- HTTP response code
            status {str} -- Computer-friendly error status
            message {str} -- Human-friendly error message
        """

        super().__init__()

        self.code = code
        self.status = status
        self.message = message

    def response(self) -> Response:
        """Create flask.Response object

        Returns:
            Response -- A flask.Response containing the error details.
        """

        response = jsonify(
            {"ok": False, "status": self.status, "message": self.message}
        )
        response.status_code = self.code

        return response
