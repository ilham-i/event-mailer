# -*- coding: utf-8 -*-
"""
    mailer.models
    ~~~~~~~~~~~~~~~~~~~

    Database connection and models.

    :copyright: © 2018 by Ilham Imaduddin.
"""

from mailer.models.db import db
from mailer.models.email import Email
from mailer.models.event import Event
