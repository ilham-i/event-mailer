# -*- coding: utf-8 -*-
"""
    mailer.api.email
    ~~~~~~~~~~~~~~~~

    Request handler for Save Email API.

    :copyright: © 2018 by Ilham Imaduddin.
"""
# pylint: disable=no-self-use

from typing import Dict

from dateutil.parser import parse
from flask import Response, request, jsonify
from peewee import IntegrityError, PeeweeException

from mailer.api.base import RequestData
from mailer.api.exception import RequestError
from mailer.models.email import Email
from mailer.tasks.celery import send_email


class SaveEmailData(RequestData):
    """Validated data of the Save Email API."""

    schema = {
        "type": "object",
        "properties": {
            "event_id": {"type": "number"},
            "email_subject": {"type": "string"},
            "email_content": {"type": "string"},
            "timestamp": {"type": "string"},
        },
        "required": ["event_id", "email_subject", "email_content", "timestamp"],
    }

    def __init__(self, data: Dict[str, str]) -> None:
        self.event_id = None
        self.email_subject = None
        self.email_content = None
        self.timestamp = None

        self.load(data, request_name="save_emails")


class SaveEmailHandler(object):
    """Handle request to save and enqueue emails for sending."""

    @staticmethod
    def handle_request() -> Response:
        """Validate incoming request and return a reponse."""

        try:
            handler = SaveEmailHandler()
            response = handler.save()

        except RequestError as err:
            response = err.response()

        return response

    def save(self) -> Response:
        """Handle save request."""

        data = SaveEmailData(request.get_json())
        email = self.__create_email(data)
        self.__enqueue_email(email)

        return jsonify(
            {
                "ok": True,
                "status": "save_emails/success",
                "message": f"The email has been scheduled to be sent at {email.send_at}.",
            }
        )

    def __create_email(self, data: SaveEmailData) -> Email:
        try:
            return Email.create(
                event_id=data.event_id,
                subject=data.email_subject,
                content=data.email_content,
                send_at=data.timestamp,
            )
        except IntegrityError:
            raise RequestError(
                400,
                "save_emails/event-not-exist",
                f"Event with id {data.event_id} does not exist.",
            )
        except PeeweeException:
            raise RequestError(
                500,
                "save_emails/save-error",
                "An error occured when trying to save email.",
            )

    def __enqueue_email(self, email: Email):
        """Enqueue email for sending

        There are two ways to enqueue the email for sending.
        1. Create one task for multiple email
        2. Create multiple task, each sends one email

        SMTP is slow. Since Celery runs in multiple workers, my hypothesis is
        that sending the email in multiple worker is better. This method
        implements the number 2.

        But,
        - I havem't test it with real data
        - An SMTP provider might limits the number of concurrent connection,
          I haven't done any research on this

        Arguments:
            email {Email} -- The email to send
        """

        for recipient_id, _ in enumerate(email.event.recipients):
            send_email.apply_async((email.id, recipient_id), eta=parse(email.send_at))
