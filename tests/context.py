# -*- coding: utf-8 -*-
"""
    tests.api.context
    ~~~~~~~~~~~~~~~~~

    Test context.

    :copyright: © 2018 by Ilham Imaduddin.
"""

import os

os.environ["DB_NAME"] = "postgres"
os.environ["DB_USER"] = "postgres"
os.environ["DB_PASSWORD"] = "password"
os.environ["DB_HOST"] = "localhost"
os.environ["CELERY_BROKER"] = "pyamqp://guest:guest@rabbitmq//"
os.environ["EMAIL_SMTP_HOST"] = "smtp.googlemail.com"
os.environ["EMAIL_SMTP_PORT"] = "587"
os.environ["EMAIL_SMTP_LOGIN"] = "some@gmail.com"
os.environ["EMAIL_SMTP_PASSWORD"] = "somepassword"
os.environ["EMAIL_FROM_ADDRESS"] = "some@gmail.com"
os.environ["EMAIL_FROM_NAME"] = "Name"
