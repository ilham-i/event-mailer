# Event Mailer

[![pipeline status](https://gitlab.com/ilhamadun/event-mailer/badges/master/pipeline.svg)](https://gitlab.com/ilhamadun/event-mailer/commits/master)
[![coverage report](https://gitlab.com/ilhamadun/event-mailer/badges/master/coverage.svg)](https://gitlab.com/ilhamadun/event-mailer/commits/master)

## Usage

### Configuration

Create a configuration file on `.env`. The fully configured `.env` looks like the following example. You can also pass these values on environment variable.

```
DB_NAME=postgres
DB_USER=postgres
DB_PASSWORD=password
DB_HOST=postgres
CELERY_BROKER=pyamqp://
EMAIL_SMTP_HOST=smtp.googlemail.com
EMAIL_SMTP_PORT=587
EMAIL_SMTP_LOGIN=some@gmail.com
EMAIL_SMTP_PASSWORD=somepassword
EMAIL_FROM_ADDRESS=some@gmail.com
EMAIL_FROM_NAME=Name
```

### Running the service

The following command will run all services and initialize the database. It includes all dependencies such as PostgreSQL and RabbitMQ.

```
docker-compose up
```

## API

### Create Event

```
POST /event
```

#### Arguments

Send the request data as `application/json`.

| Argument     | Example              | Description              |
| ------------ | -------------------- | ------------------------ |
| `category`   | `"announcement"`     | Category of the event    |
| `recipients` | `["some@email.com"]` | Array of event recipient |

#### Response

| Key       | Type      | Desscription                                  |
| --------- | --------- | --------------------------------------------- |
| `ok`      | `boolean` | Whether the request is success or not.        |
| `status`  | `string`  | Computer-readable status.                     |
| `message` | `string`  | Human-readable message.                       |
| `event`   | `object`  | Value of the Event. Only when `ok` is `true`. |

#### Error Codes

| Code                     | Description                                   |
| ------------------------ | --------------------------------------------- |
| `event/validation-error` | Request data is invalid.                      |
| `event/create-error`     | An error occured when trying to create Event. |

#### Request Example

```json
{
    {
        "category": "announcement",
        "recipients": ["some@email.com"]
    }
}
```

#### Success Response Example

```json
{
    "ok": true,
    "status": "event/create-success",
    "message": "Event has been successfully created.",
    "event": {
        "category": "newsletter",
        "created_at": "Wed, 27 Jun 2018 02:31:06 GMT",
        "id": 1,
        "recipients": ["some@email.com"]
    }
}
```

#### Error Response Example

```json
{
    "ok": false,
    "status": "event/validation-error",
    "message": "The recipients are required."
}
```

### Save Emails

Save email and enqueue for sending.

```
POST /save_emails
```

#### Arguments

Send the request data as `application/json`.

| Argument        | Example                       | Description                                                        |
| --------------- | ----------------------------- | ------------------------------------------------------------------ |
| `event_id`      | `1`                           | Id of the event.                                                   |
| `email_subject` | `A Subject`                   | Email subject.                                                     |
| `email_content` | `A Content`                   | Email content.                                                     |
| `timestamp`     | `"2018-06-24T11:41:00+08:00"` | Time when the email should be sent, Must be formatted as ISO 8601. |

#### Response

| Key       | Type      | Desscription                           |
| --------- | --------- | -------------------------------------- |
| `ok`      | `boolean` | Whether the request is success or not. |
| `status`  | `string`  | Computer-readable status.              |
| `message` | `string`  | Human-readable message.                |

#### Error Codes

| Code                           | Description                                 |
| ------------------------------ | ------------------------------------------- |
| `save_emails/validation-error` | Request data is invalid.                    |
| `save_emails/event-not-exist`  | Event with the given id does not exist.     |
| `save_emails/save-error`       | An error occured when trying to save email. |

#### Request Example

```json
{
    {
        "event_id": 1,
        "email_subject": "A Subject",
        "email_content": "Hello World!",
        "timestamp": "2018-06-24T11:41:00+08:00"
    }
}
```

#### Success Response Example

```json
{
    "ok": true,
    "message":
        "The email has been scheduled to be sent at 2018-06-24T11:41:00+08:00.",
    "status": "save_emails/success"
}
```

#### Error Response Example

```json
{
    "ok": false,
    "status": "event/validation-error",
    "message": "The email is required."
}
```
