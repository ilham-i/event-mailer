# -*- coding: utf-8 -*-
"""
    mailer.tasks.email_sender
    ~~~~~~~~~~~~~~~~~~~~~~~~~

    SMTP wrapper that manages connection stack on multithreaded environment.

    :copyright: © 2018 by Ilham Imaduddin.
"""

import os

from envelopes import Envelope, SMTP, connstack

from mailer.models import Email, Event


class EmailSender(object):
    """EmailSender wraps SMTP to maintain it's connection stack.

    Use the EmailSender within a context manager. Each context will be
    provided with an SMTP connection, so it's safe to use in a multithreaded
    environment.

    Example:
    ```python
    with EmailSender() as postman:
        postman.send(1, 1)
    ```
    """

    __host = os.environ["EMAIL_SMTP_HOST"]
    __port = os.environ["EMAIL_SMTP_PORT"]
    __login = os.environ["EMAIL_SMTP_LOGIN"]
    __password = os.environ["EMAIL_SMTP_PASSWORD"]
    __from_address = os.environ["EMAIL_FROM_ADDRESS"]
    __from_name = os.environ["EMAIL_FROM_NAME"]

    __connection = SMTP(__host, __port, __login, __password, tls=True)

    def __enter__(self):
        connstack.push_connection(self.__connection)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        connstack.pop_connection()

    def send(self, email_id: int, recipient_id: int):
        """Send email of `email_id` to recipient of `recipient_id`."""

        email = self.__get_email(email_id, recipient_id)

        envelope = Envelope(
            from_addr=(self.__from_address, self.__from_name),
            to_addr=email.recipient,
            subject=email.subject,
            text_body=email.content,
        )

        smtp = connstack.get_current_connection()
        smtp.send(envelope)

    # pylint: disable=no-self-use
    def __get_email(self, email_id: int, recipient_id: int) -> Email:
        return (
            Email.select(  # pylint: disable=no-member
                Email.subject,
                Email.content,
                Event.recipients[recipient_id].alias("recipient"),
            )
            .join(Event)
            .where(Email.id == email_id)
            .get()
        )
