# -*- coding: utf-8 -*-
"""
    tests.api.test_event
    ~~~~~~~~~~~~~~~~~~~~

    Test cases for Event API handler.

    :copyright: © 2018 by Ilham Imaduddin.
"""
# pylint: disable=no-self-use,redefined-outer-name

from unittest.mock import MagicMock

import pytest
from peewee import PeeweeException

import tests.context  # pylint: disable=unused-import
from tests.api.client import client  # pylint: disable=unused-import
from mailer.api.exception import RequestError
from mailer.api.event import EventPOSTData
from mailer.models.event import Event


class TestEventPostData(object):
    """Test cases for EventPostData."""

    def test_valid_data(self):
        """EventPOSTData should contains the same data with it's raw data."""
        raw_data = {"recipients": ["some@email.com"], "category": "announcement"}

        data = EventPOSTData(raw_data)

        assert data.recipients == raw_data["recipients"]
        assert data.category == raw_data["category"]

    def test_invalid_data(self):
        """Invalid data should raise RequestError."""
        raw_data = {"recipients": ["some@email.com"]}

        with pytest.raises(RequestError) as execinfo:
            EventPOSTData(raw_data)

        assert execinfo.value.code == 400
        assert execinfo.value.status == "event/validation-error"
        assert "category" in execinfo.value.message

    def test_invalid_email(self):
        """EventPOSTData should contains the same data with it's raw data."""
        raw_data = {"recipients": ["someemail.com"], "category": "announcement"}

        with pytest.raises(RequestError) as execinfo:
            EventPOSTData(raw_data)

        assert execinfo.value.code == 400
        assert execinfo.value.status == "event/validation-error"
        assert execinfo.value.message == "Recipient's email address is invalid."

    def test_invalid_data_type(self):
        """Invalid data type should raise RequestError."""
        raw_data = {"recipients": "some@email.com", "category": "announcement"}

        with pytest.raises(RequestError) as execinfo:
            EventPOSTData(raw_data)

        assert execinfo.value.code == 400
        assert execinfo.value.status == "event/validation-error"
        assert "array" in execinfo.value.message


class ListMock(object):  # pylint: disable=missing-docstring,too-few-public-methods
    def execute(self):  # pylint: disable=missing-docstring
        return [
            Event(recipients=["some@email.com"], category="some-category"),
            Event(recipients=["some@email.com"], category="some-category"),
        ]


class TestEventHandler(object):
    """Test cases for EventHandler."""

    def test_list(self, client):
        """Should list Events."""
        Event.select = MagicMock(return_value=ListMock())

        response = client.get("/event")
        response_data = response.get_json()

        assert response.status_code == 200
        assert response_data["ok"] is True
        assert response_data["status"] == "event/list-success"
        assert response_data["message"] == "Event has been successfully listed."
        assert len(response_data["events"]) == 2

        for event in response_data["events"]:
            assert event["recipients"] == ["some@email.com"]
            assert event["category"] == "some-category"

    def test_list_internal_error(self, client):
        """Should return 500."""
        Event.select = MagicMock(side_effect=PeeweeException())

        response = client.get("/event")
        response_data = response.get_json()

        assert response.status_code == 500
        assert response_data["ok"] is False
        assert response_data["status"] == "event/list-error"
        assert response_data["message"] == "An error occured when trying to list Event."

    def test_get(self, client):
        """Should get one Event."""
        Event.get_by_id = MagicMock(
            return_value=Event(recipients=["some@email.com"], category="some-category")
        )

        response = client.get("/event/1")
        response_data = response.get_json()

        assert Event.get_by_id.call_args[0][0] == 1
        assert response.status_code == 200
        assert response_data["ok"] is True
        assert response_data["status"] == "event/get-success"
        assert response_data["message"] == "Event has been successfully fetched."
        assert response_data["event"]["recipients"] == ["some@email.com"]
        assert response_data["event"]["category"] == "some-category"

    def test_get_not_found(self, client):
        """Should return 404."""
        Event.get_by_id = MagicMock(return_value=None)

        response = client.get("/event/1")
        response_data = response.get_json()

        assert Event.get_by_id.call_args[0][0] == 1
        assert response.status_code == 404
        assert response_data["ok"] is False
        assert response_data["status"] == "event/event-not-found"
        assert response_data["message"] == "Event with id 1 does not exist."

    def test_get_invalid_key(self, client):
        """Should return 404."""
        Event.get_by_id = MagicMock(return_value=None)

        response = client.get("/event/asd")
        response_data = response.get_json()

        assert response.status_code == 404
        assert response_data["ok"] is False
        assert response_data["status"] == "event/event-not-found"
        assert response_data["message"] == "Event with id asd does not exist."

    def test_get_internal_error(self, client):
        """Internal error should return 500."""
        Event.get_by_id = MagicMock(side_effect=PeeweeException())

        response = client.get("/event/1")
        response_data = response.get_json()

        assert response.status_code == 500
        assert response_data["ok"] is False
        assert response_data["status"] == "event/get-error"
        assert response_data["message"] == "An error occured when trying to get Event."

    def test_create(self, client):
        """Should create an event."""
        request_data = {"recipients": ["some@email.com"], "category": "some-category"}
        Event.create = MagicMock(return_value=Event(**request_data))

        response = client.post("/event", json=request_data)
        response_data = response.get_json()

        assert Event.create.call_args[1]["recipients"] == request_data["recipients"]
        assert Event.create.call_args[1]["category"] == request_data["category"]
        assert response.status_code == 201
        assert response_data["ok"] is True
        assert response_data["status"] == "event/create-success"
        assert response_data["message"] == "Event has been successfully created."
        assert response_data["event"]["recipients"] == request_data["recipients"]
        assert response_data["event"]["category"] == request_data["category"]

    def test_create_invalid_data(self, client):
        """Invalid create data should returns validation error."""
        request_data = {"recipients": "some@email.com", "category": "some-category"}
        response = client.post("/event", json=request_data)
        response_data = response.get_json()

        assert response.status_code == 400
        assert response_data["ok"] is False
        assert response_data["status"] == "event/validation-error"
        assert "array" in response_data["message"]

    def test_create_internal_error(self, client):
        """Internal error should return 500."""
        request_data = {"recipients": ["some@email.com"], "category": "some-category"}
        Event.create = MagicMock(side_effect=PeeweeException())

        response = client.post("/event", json=request_data)
        response_data = response.get_json()

        assert response.status_code == 500
        assert response_data["ok"] is False
        assert response_data["status"] == "event/create-error"
        assert (
            response_data["message"] == "An error occured when trying to create Event."
        )
